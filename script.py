
# a = "I was tired, I am man, who are you? what the fuck. hey hey"
# print(a.split([' ']).count('man'))


import re
import pandas as pd
from nltk import pos_tag
import string
# s = '# #Trump2020'
# s = re.sub('\#[a-zA-Z0-9]*','',s)
# print(s)
#
# DataFrame = pd.read_csv('OLID/test_a.csv')
# DataFrame['subtask_a'] = DataFrame['subtask_a'].map(lambda x: '?')
# DataFrame.to_csv('weka_test_a.csv',index=None)

from torch import nn, optim
import torch.nn.functional as F
import torch.utils.data as data
import torch
import numpy as np

# train = pd.read_csv("final_train_a.csv")
# n = 10
# img_name = train.iloc[:n, 0]
# landmarks = train.iloc[:n, 1]
# print('Image name:\n {}'.format(img_name))
# print('First 4 Landmarks:\n {}'.format(landmarks[:4]))

def tag_part_of_speech(text):
    text_splited = text.split(' ')
    text_splited = [''.join(c for c in s if c not in string.punctuation) for s in text_splited]
    text_splited = [s for s in text_splited if s]
    pos_list = pos_tag(text_splited)
    print(pos_list)
    noun_count = len([w for w in pos_list if w[1] in ('PRP')])
    adjective_count = len([w for w in pos_list if w[1] in ('JJ','JJR','JJS')])
    verb_count = len([w for w in pos_list if w[1] in ('VB','VBD','VBG','VBN','VBP','VBZ')])
    return[noun_count, adjective_count, verb_count]


map = {'IND': 0, 'GRP': 1, 'OTH': 2}
s = 'www, OTH:2'
print(map[''])