from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix

import numpy as np
# OFF和NOT的真实数量
OFF = 240
NOT = 620
SUM = OFF + NOT

confus_matrix_OFF = []
#初始化真实值和预测值的数组，初始数组元素均为0(NOT)
y_true = np.zeros(SUM)
y_pred = np.zeros(SUM)

# OFF标记为1, NOT标记为0，
y_true[:OFF] = 1 # y_true数组的0-OFF数量的元素改为1(OFF)
y_pred[:]    = 0 # y_pred的元素全部设为0，代表全部预测为NOT

print("F1-[NOT:OFF]: -> ", f1_score(y_true, y_pred, average=None))
print("F1-[binary]: -> ", f1_score(y_true, y_pred, pos_label=1))
print("F1-[weighted]: -> ", f1_score(y_true, y_pred,average='weighted'))
print("F1-[micro]: -> ", f1_score(y_true, y_pred,average='micro'))
print("F1-[macro]: -> ", f1_score(y_true, y_pred,average='macro'))

print("Confusion Marix:\n", confusion_matrix(y_true, y_pred))