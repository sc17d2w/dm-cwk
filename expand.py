import contractions
import pandas as pd
from autocorrect import Speller
from time import time
import re

check = Speller(lang='en')

path = 'OLID/olid-training-v1.0.tsv'
try:
    csvf = pd.read_csv(path,sep='\t')
    encode = 'utf-8'
except UnicodeDecodeError:
    csvf = pd.read_csv(path, encoding='ISO-8859-1')
    encode = 'ISO-8859-1'




csvf['tweet'] = csvf['tweet'].str.replace('@USER','')
csvf['tweet'] = csvf['tweet'].str.replace('URL','')
# # 替换所有#开头的twitter主题字符
# csvf['tweet'] = csvf['tweet'].map(lambda x: re.sub('\#[a-zA-Z0-9]*', '', x))
csvf['tweet'] = csvf['tweet'].str.replace('[^\w\s]','')
# 缩写展开 i've -> i have
csvf['tweet'] = csvf['tweet'].map(lambda x: contractions.fix(x))
start = time()
# 拼写检查
csvf['tweet'] = csvf['tweet'].map(lambda x: check(x))
stop = time()
print(stop-start)
csvf.to_csv('fix_a.csv',index=None)

