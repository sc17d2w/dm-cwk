import torch
from torch import nn
import string
import gensim
from torch import autograd

class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()

        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(combined)
        output = self.i2o(combined)
        output = self.softmax(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, self.hidden_size)



def wv(model, word):
    return model[word]

all_letters = string.ascii_letters + " .,;'"
n_letters = len(all_letters)

learning_rate = 0.05
n_hidden = 128
rnn = RNN(n_letters, n_hidden, 2)
criterion = nn.NLLLoss()
def train(category_tensor, line_tensor):
    hidden = rnn.initHidden()

    rnn.zero_grad()

    for i in range(line_tensor.size()[0]):
        output, hidden = rnn(line_tensor[i], hidden)

    loss = criterion(output, category_tensor)
    loss.backward()

    # Add parameters' gradients to their values, multiplied by learning rate
    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output, loss.item()


if __name__ == '__main__':
    # model = gensim.models.KeyedVectors.load_word2vec_format('olid.bin')
    # print(model['user'])
    x = torch.tensor([1,2,3,4,5,6,7])
    print(x)
    x = autograd.Variable(x)
    print('change\n')
    print(x)

