import pandas as pd
from gensim.models import Word2Vec
import matplotlib.pyplot as plt
from nltk import FreqDist
from nltk import TweetTokenizer
import re

def count(data, entry):
    return data[entry].value_counts()

def build_doc(data, entry):
    return data[entry]

data= pd.read_csv('OLID/test_a.csv', encoding = "ISO-8859-1")
print(count(data, 'subtask_a'))
docs = build_doc(data,'tweet')

from nltk.tokenize import RegexpTokenizer


def filter_video(comments):
    tknzr = TweetTokenizer()

    token_words = []
    for i in comments:
        token_words.append(tknzr.tokenize(i))

    # Combine all the comments
    all_words = []
    for j in token_words:
        for k in j:
            all_words.append(k)
    # print(all_words)

    clean_words = []
    # tokenizer = RegexpTokenizer(r'\w+')
    for word in all_words:
        clean_words.append(word)
    clean_words = list(filter(None, clean_words))

    #     Remove stop words
    filtered_sentence = []

    for w in clean_words:
            #             w = wordnet_lemmatizer.lemmatize(w)
        filtered_sentence.append(w)
    return filtered_sentence
# docs = docs.str.lower()
# docs = docs.str.replace('[^\w\s]','')
# print(docs.head(5000))
texts = [[word for word in doc.lower().split()] for doc in docs]
# # print(texts)
#
# model = Word2Vec(texts, size=50, window=10, min_count=1, workers=4, iter=10, sg=1, hs=1, alpha=0.015)
#
# model.wv.save_word2vec_format('olid.bin')
# vectors = model.wv
# vectors.save('olid_wv')

# task_a = data[['id','tweet','subtask_a']]
# a = task_a.copy()
# off_a = a[a['subtask_a'] == 'OFF']
# off_frequency = FreqDist(filter_video(off_a['tweet']))
# print(off_frequency.most_common(100))